package Task_2;

import Task_2.Commands.GoCommand;
import Task_2.Commands.LeftCommand;
import Task_2.Commands.RightCommand;
import Task_2.Commands.StopCommand;
import java.util.Scanner;

public class Main {

  public static void main(String[] args) {
    CommandManager commandManager = new CommandManager();
    Scanner scanner = new Scanner(System.in);
    String input;
    String arg;
    do {
      System.out.println("Enter: ");
      System.out.println("commands:GO, STOP, RIGHT, LEFT, EXIT");
      input = scanner.next();
      switch (input){
        case "GO": commandManager.setCommand(new GoCommand());
        executing(commandManager,scanner);
        break;
        case "STOP": commandManager.setCommand(new StopCommand());
        executing(commandManager,scanner);
        break;
        case "RIGHT": commandManager.setCommand(new RightCommand());
        executing(commandManager,scanner);
        break;
        case "LEFT": commandManager.setCommand(new LeftCommand());
        executing(commandManager,scanner);
        break;
        case "EXIT": System.exit(0);
        default:
          System.out.println("Enter correct command!");
          break;
      }

    }
    while (true);
  }
  private static void executing(CommandManager commandManager, Scanner scanner){
    System.out.println("Enter arg now: ");
    String arg = scanner.next();
    System.out.println("EXECUTING!");
    commandManager.execute(arg);
  }

}
