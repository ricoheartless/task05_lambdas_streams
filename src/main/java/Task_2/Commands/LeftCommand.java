package Task_2.Commands;

import Task_2.Commands.Command;

/**
 * Abstract class approach
 */
public class LeftCommand implements Command {
  private Command command = new Command() {
    @Override
    public void make(String arg) {
      System.out.println("Left: "+arg);
    }
  };
  @Override
  public void make(String arg) {
    command.make(arg);
  }

}
