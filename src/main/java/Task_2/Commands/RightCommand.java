package Task_2.Commands;

/**
 * Object reference method approach
 */
public class RightCommand implements Command {

  @Override
  public void make(String arg) {
    RightCommand rightCommand = new RightCommand();
    rightCommand.goRight(arg);
  }
  private void goRight(String arg){
    System.out.println("Right: "+arg);
  }
}
