package Task_2.Commands;

import Task_2.Commands.Command;

/**
 *  Lambda approach
 */
public class GoCommand implements Command {

  @Override
  public void make(String arg) {
    Command command = (a) -> {
      System.out.println("Going: " + arg);
    };
    command.make(arg);
  }
}
