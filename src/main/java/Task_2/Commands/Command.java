package Task_2.Commands;
@FunctionalInterface
public interface Command {
  void make(String arg);
}
