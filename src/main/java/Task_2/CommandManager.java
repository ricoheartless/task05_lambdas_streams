package Task_2;

import Task_2.Commands.Command;

public class CommandManager {
  Command command;
  CommandManager(){
  }
  CommandManager(Command command){
    this.command = command;
  }
  public void execute(String arg){
    command.make(arg);
  }
  public void setCommand(Command command){
    this.command = command;
  }
}
