package Task_3;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class RandomList {
  RandomList(){}
  public static List<Integer> getRandomList(int capacity, int min, int max){
    List<Integer> list = Collections.nCopies(capacity,1);
    list = list.stream().map(i -> getNum(min,max)).collect(Collectors.toList());
    return list;
  }
  private static int getNum(int min,int max){
    Random random = new Random();
    int num = random.nextInt((max - min)+1)+min;
    return num;
  }
}
